# Pagan calendar iCal file generator

A script which generates calendars containing solstices, equinoxes and the Pagan festivals inbetween them.

## Terminology

* Solstices - these are astronomical, religious and also practical as they are the longest day and night.
* Equinoxes - equal length day and night, and when the day length is changing fastest.
* Quarter days - these divide the year into four - they are the solstices and the equinoxes combined.
* Cross-quarter days - are roughly half way between a solstice and an equinox, on the first of the month for modern Pagans.
* Astronomical cross-quarter days - the actual time half way between a solstice and an equinox.

## Generating the calendar

Run the following commands:

```
pipenv install
pipenv run ./makecal.py
```

This is hard coded to:
* write to the file `pagan.ics`.
* generate all entries from 2000 to 2099, as that is still quite a small file.
* include the solstices, equinoxes and the first of the month cross-quarter days.
* use all day events, and assumes a UK timezone to work out the date from astronomical time.
* assume northern hemisphere.


## TODO

* Parse command line parameters to customise what to include
* Add astronomical cross-quarter days (for Helen)
* Allow for times of sunrise/sunset based on latitude (right now assumes the date of, for example, the summer solstice is the same day of the time the earth is tilted towards the sun) - use Astral https://astral.readthedocs.io/en/latest/index.html
* Southern hemisphere

